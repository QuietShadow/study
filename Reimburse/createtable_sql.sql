﻿create database ReimburseDB default character set utf8;
use ReimburseDB;

drop table if exists t_employee ;
drop table if exists t_Reimburse ;
drop table if exists t_ReimburseType;

create table T_Employee(
	eId varchar(10) primary key, 
	eDept varchar(15) not null, 
	eName varchar(15) not null, 
	eMGR varchar(10) references t_employe(eid),
	eJob varchar(15) not null default '普通职员', 
	check(eJob in ('部长','副部长','普通职员','总经理')) 
) default character set utf8 ;

create table T_ReimburseType( 
	tId varchar(15)  primary key,
	tName varchar(15) not null,
	perHoliday float(2,1) not null  default 1,
	perManager float(2,1) not null   default 1,  
	perDeManager float(2,1) not null default 1,
	check(perManager >=1 and perManager <=2), 
	check(perHoliday >=1 and perHoliday <=2) , 
	check(perDeManager  >=1 and perDeManager  <=2)
) default character set utf8;

create table T_Reimburse( 
	rId int(10) primary key auto_increment, 
	rDate Date  not null, 
	rEmp varchar(10)  not null  references t_employe(eid), 
	rAddr varchar(100), 
	rProvince varchar(10)  not null, 
	rType varchar(15)  not null  references T_ReimburseType(tid), 
	rSum float(10,2) not null check (rSum >=0),
	rActSum float(10,2) not null check (rActSum >=0)
) default character set utf8; 

load data local infile 'd:/reimburse/employee.txt' into table t_employee fields terminated by '\t';
load data local infile 'd:/reimburse/reimbursetype.txt' into table T_ReimburseType fields terminated by '\t';
load data local infile 'd:/reimburse/reimburse.txt' into table T_Reimburse fields terminated by '\t';


SET character_set_client =gbk;      //设置客服端的编码


SET character_set_results =gbk;      //设置服务器端结果返回的编码


SET character_set_connection =gbk;   //设置客服端与服务端连接时的编码

